from flask_restful import fields

user_fields = {
    'id': fields.Integer,
    'email': fields.String,
    'first_name': fields.String,
    'last_name': fields.String,
}

project_list_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'creator': fields.Nested(user_fields)
}

project_detail_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'creator': fields.Nested(user_fields),
    'invitations': fields.List(fields.Nested({
        'id': fields.Integer,
        'requesting_user': fields.Nested(user_fields),
        'receiving_user': fields.Nested(user_fields),
        'status': fields.String(
            attribute=lambda invitation:
            str(invitation.status).replace('InvitationStatus.', ''))

    }))
}
