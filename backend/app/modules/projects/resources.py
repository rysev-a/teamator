from flask_restful import Resource, request, marshal
from flask_login import current_user
from app.crud import ListResource, DetailResource
from app.core.database import db
from .fields import project_detail_fields, project_list_fields
from .models import Project
from ..invitations.models import Invitation, InvitationStatus


class ProjectList(ListResource):
    fields = project_list_fields
    model = Project


class ProjectDetail(DetailResource):
    fields = project_detail_fields
    model = Project


class ProjectCreate(Resource):
    def post(self):

        project = Project(
            creator_id=current_user.id,
            name=request.json.get('name'),
            description=request.json.get('description')
        )

        db.session.add(project)

        for invite_user in request.json.get('invite_users'):
            invitation = Invitation(
                requesting_user_id=current_user.id,
                receiving_user_id=invite_user.get('value'),
                status=InvitationStatus.pending,
                project=project
            )

            db.session.add(invitation)

        db.session.commit()

        return {'message': 'ok'}


class ProjectUpdate(Resource):
    def put(self, id):
        query = Project.query.filter_by(id=id)
        project = Project.query.get(id)

        self.update_invitations(request.json, project)
        self.update_members(request.json, project)

        # update main info
        query.update(request.json)
        db.session.commit()
        return marshal(query.first(), project_detail_fields)

    @staticmethod
    def update_invitations(request_json, project):
        invite_users = request_json.get('invite_users')
        if invite_users or len(invite_users) == 0:
            user_ids = set([
                user.get('value') for user in invite_users
            ])

            prev_user_ids = set(
                [inv.receiving_user_id for inv in project.invitations
                    if inv.status == InvitationStatus.pending])

            # remove invitations
            query = Invitation.query.filter(
                # only for current project
                Invitation.project_id == project.id,
                # only for pending status
                Invitation.status == InvitationStatus.pending,
                # if prev id not in new ids
                Invitation.receiving_user_id.in_(
                    list(prev_user_ids - user_ids)
                )
            ).delete(synchronize_session='fetch')

            # add new invitations
            for user_id in user_ids - prev_user_ids:
                db.session.add(Invitation(
                    project_id=project.id,
                    requesting_user_id=project.creator_id,
                    receiving_user_id=user_id
                ))

            del request_json['invite_users']

    @staticmethod
    def update_members(request_json, project):
        member_ids = set([
            member.get('value') for member in request_json.get('members')])

        prev_member_ids = set(
            [inv.receiving_user_id for inv in project.invitations
             if inv.status == InvitationStatus.success])

        # remove invitations
        query = Invitation.query.filter(
            # only for current project
            Invitation.project_id == project.id,
            # only for success status
            Invitation.status == InvitationStatus.success,
            # if prev id not in new ids
            Invitation.receiving_user_id.in_(
                list(prev_member_ids - member_ids)
            )
        ).delete(synchronize_session='fetch')

        del request_json['members']
