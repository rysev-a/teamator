from app.core.api import api
from .resources import CypressClear, CypressReload


def init():
    api.add_resource(CypressClear, '/api/v1/cypress/clear')
    api.add_resource(CypressReload, '/api/v1/cypress/reload')
