from flask_restful import request, marshal
from app.crud import ListResource, DetailResource
from app.core.database import db
from .fields import invitation_detail_fields, invitation_list_fields
from .models import Invitation


class InvitationList(ListResource):
    fields = invitation_list_fields
    model = Invitation


class InvitationDetail(DetailResource):
    fields = invitation_detail_fields
    model = Invitation
