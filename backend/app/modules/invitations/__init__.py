from app.core.api import api
from .resources import InvitationDetail, InvitationList


def init():
    api.add_resource(InvitationList, '/api/v1/invitations')
    api.add_resource(InvitationDetail, '/api/v1/invitations/<int:id>')
