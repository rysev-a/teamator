import enum
from app.core.database import db


class InvitationStatus(enum.Enum):
    pending = 1
    success = 2
    rejected = 3


class InvitationType(enum.Enum):
    from_member = 1
    from_user = 2


class Invitation(db.Model):
    __tablename__ = 'invitations'

    id = db.Column(db.Integer, primary_key=True)

    requesting_user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    requesting_user = db.relationship(
        'User', foreign_keys=[requesting_user_id])
    receiving_user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    receiving_user = db.relationship('User', foreign_keys=[receiving_user_id])

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'))
    project = db.relationship('Project')

    type = db.Column(db.Enum(InvitationType),
                     default=InvitationType.from_member)

    status = db.Column(db.Enum(InvitationStatus),
                       default=InvitationStatus.pending)
