import click
from flask.cli import AppGroup
from app.core.database import db
from app.utils import generate_from_mock
from .models import Invitation

invitation_cli = AppGroup('invitation')
role_cli = AppGroup('role')


@invitation_cli.command('generate')
def generate_invitations():
    generate_from_mock(mock='invitations', model=Invitation, db=db)


@invitation_cli.command('clear')
def clear():
    Invitation.query.delete()
    db.session.commit()
