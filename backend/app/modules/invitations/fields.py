from flask_restful import fields
from .models import InvitationStatus

user_fields = {
    'id': fields.Integer,
    'email': fields.String,
    'first_name': fields.String,
    'last_name': fields.String
}

project_fields = {
    'id': fields.Integer,
    'name': fields.String
}

invitation_list_fields = {
    'id': fields.Integer,
    'receiving_user': fields.Nested(user_fields),
    'requesting_user': fields.Nested(user_fields),
    'project': fields.Nested(project_fields),
    'status': fields.String(
        attribute=lambda invitation:
        str(invitation.status).replace('InvitationStatus.', '')),
    'type': fields.String(
        attribute=lambda invitation:
        str(invitation.type).replace('InvitationType.', ''))
}

invitation_detail_fields = {
    'id': fields.Integer,
    'receiving_user': fields.Nested(user_fields),
    'requesting_user': fields.Nested(user_fields),
    'project': fields.Nested(project_fields),
    'status': fields.String
}
