from flask import Flask

from app.core.api import api
from app.core.database import db
from app.core.migrate import migrate
from app.core.authorization import login_manager
from app.core.middlewares import add_delay

# import app modules
from app.modules import modules


def create_app(settings):
    app = Flask(__name__)
    app.config.from_object(settings)

    # init app modules
    modules.init_app(app)

    # only for development (delay middleware)
    add_delay(app)

    # init extensions
    db.init_app(app)
    migrate.init_app(app, db)
    api.init_app(app)
    login_manager.init_app(app)

    return app
