import time


def add_delay(app):
    @app.before_request
    def before_request():
        time.sleep(app.config.get('REQUEST_DELAY', 0))
