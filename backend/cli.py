import os
import click
from app import create_app
from app.core.database import db
from app.modules.users.cli import user_cli, role_cli
from app.modules.projects.cli import project_cli
from app.modules.invitations.cli import invitation_cli

from app.modules.users.models import User
from app.modules.projects.models import Project
from app.modules.invitations.models import Invitation, InvitationStatus

SETTINGS_ENV = os.environ.get('SETTINGS_ENV') or 'app.settings.development'
app = create_app(SETTINGS_ENV)
app.cli.add_command(user_cli)
app.cli.add_command(role_cli)
app.cli.add_command(project_cli)
app.cli.add_command(invitation_cli)


@app.cli.command()
@click.pass_context
def reload(ctx):
    db.drop_all()
    db.create_all()
    commands = [
        role_cli.get_command('role', 'generate'),
        user_cli.get_command('user', 'generate'),
        project_cli.get_command('project', 'generate'),
        invitation_cli.get_command('invitations', 'generate')
    ]

    for command in commands:
        ctx.invoke(command)


@app.cli.command()
def clear():
    db.drop_all()
    db.create_all()


@app.shell_context_processor
def make_shell_context():
    return dict(
        app=app,
        db=db,
        User=User,
        Invitation=Invitation,
        InvitationStatus=InvitationStatus,
        Project=Project
    )
