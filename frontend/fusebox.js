const {
  FuseBox,
  WebIndexPlugin,
  CSSPlugin,
  QuantumPlugin,
} = require('fuse-box');
const { YAMLPlugin } = require('fuse-box-yaml');

const isProduction = process.env.NODE_ENV === 'production';

const fuse = FuseBox.init({
  homeDir: 'src',
  output: 'build/$name.js',
  plugins: [
    WebIndexPlugin({
      template: 'src/assets/index.html',
    }),
    CSSPlugin(),
    YAMLPlugin(),
    // insert QuantumPlugin for production
    isProduction && QuantumPlugin(),
  ],
  alias: {
    app: '~/app',
  },
  sourceMaps: true,
});

const bundle = fuse.bundle('app').instructions(' > app/index.tsx');

// run dev server for development mode
if (!isProduction) {
  fuse.dev({
    fallback: 'index.html',
    proxy: {
      '/api/*': {
        target: 'http://localhost:5000',
        changeOrigin: true,
      },
    },
    port: 4444,
  });

  bundle.hmr().watch('src/**');
}

fuse.run();
