import * as yaml from 'yamljs';

Cypress.Commands.add('clearDb', () =>
  cy.request('POST', '/api/v1/cypress/clear')
);

Cypress.Commands.add('reloadDb', () =>
  cy.request('POST', '/api/v1/cypress/reload')
);

Cypress.Commands.add('authorize', user => {
  cy.request('POST', '/api/v1/account/signin', user);
});

Cypress.Commands.add('deauthorize', () => {
  cy.request('POST', '/api/v1/account/signout');
});

Cypress.Commands.add('deleteUser', id => {
  cy.request('DELETE', `/api/v1/users/${id}`);
});

Cypress.Commands.add('loadFixtures', () => {
  const fixtures = ['projects', 'users', 'roles', 'invitations'];

  fixtures.reduce(
    (acc, fixture) =>
      acc.then(() =>
        cy
          .readFile(`../fixtures/${fixture}.yaml`)
          .then(data => cy.wrap(yaml.parse(data)).as(fixture))
      ),
    Promise.resolve()
  );
});
