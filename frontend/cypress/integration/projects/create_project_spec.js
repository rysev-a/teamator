describe('Projects', function() {
  beforeEach(function() {
    cy.loadFixtures();
  });

  before(function() {
    cy.reloadDb();
  });

  after(function() {
    cy.clearDb();
  });

  it('Create project', function() {
    cy.authorize(this.users[5]);
    cy.visit('/');
    const receivingUser = this.users[6];

    const project = {
      name: 'test project',
      description: 'test project description',
    };

    // visit create project
    cy.get('#project-dropdown .navbar-dropdown').invoke('show');
    cy.contains('Создать проект').click();
    cy.get('#project-dropdown .navbar-dropdown').invoke('hide');

    // input project params
    cy.get('[name="name"]').type(project.name);
    cy.get('[name="description"]').type(project.description, {
      delay: false,
      force: true,
    });

    // select receiving user
    cy.get('.react-select__input input')
      .click({ force: true })
      .type(receivingUser.email, { force: true });
    cy.get('[role="option"]').click();
    cy.get('#project-create-form').submit();
    cy.contains('Мои проекты');

    expect(cy.contains('Мои проекты')).to.exist;
    expect(cy.contains(project.name)).to.exist;
  });
});
