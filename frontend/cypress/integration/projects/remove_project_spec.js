describe('Projects', function() {
  beforeEach(function() {
    cy.loadFixtures();
  });

  before(function() {
    cy.reloadDb();
  });

  after(function() {
    cy.clearDb();
  });

  it('Remove project', function() {
    cy.authorize(this.users[0]);
    cy.visit('/profile/projects');
    cy.contains('Удалить').click();
    cy.contains('Пока что нет ни одного проекта');
  });
});
