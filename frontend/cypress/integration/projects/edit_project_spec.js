describe('Projects', function() {
  before(() => {
    cy.reloadDb();
  });

  beforeEach(function() {
    cy.loadFixtures();
  });

  after(function() {
    cy.clearDb();
  });

  it('Edit project main fields', function() {
    cy.authorize(this.users[0]);

    cy.visit('/profile/projects');
    cy.contains('Редактировать').click();
    cy.contains('Редактирование проекта');

    cy.get('[name="name"]')
      .should('have.value', this.projects[3].name)
      .focus()
      .type(' - 2000');

    cy.get('[name="description"]')
      .should('have.value', this.projects[3].description)
      .focus()
      .type(' - 2000');

    cy.contains('Сохранить').click();

    cy.contains(`${this.projects[3].name} - 2000`).should('exist');
    cy.contains(`${this.projects[3].description} - 2000`).should('exist');
  });
});
