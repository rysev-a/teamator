describe('Projects', function() {
  before(function() {
    cy.reloadDb();
  });

  beforeEach(function() {
    cy.loadFixtures();
  });

  after(function() {
    cy.clearDb();
  });

  it('Accept invitation', function() {
    cy.authorize(this.users[0]);
    cy.visit('/profile/invitations');

    cy.contains('Принять').click();
    expect(cy.contains('принято')).to.exist;
    cy.contains('принято').should('have.class', 'is-success');
  });

  it('Decline invitation', function() {
    cy.authorize(this.users[0]);
    cy.visit('/profile/invitations');

    cy.contains('Отклонить').click();
    expect(cy.contains('отклонено')).to.exist;
    cy.contains('отклонено').should('have.class', 'is-danger');
  });
});
