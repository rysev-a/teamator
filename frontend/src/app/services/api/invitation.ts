import axios from 'axios';
import { apiHelper } from './api-helper';
import { API_URL } from 'app/settings';

export const invitationApi = {
  accept: {
    post: id => axios.post(`${API_URL}/api/v1/invitations/${id}/accept`),
  },
  decline: {
    post: id => axios.post(`${API_URL}/api/v1/invitations/${id}/decline`),
  },
  ...apiHelper('invitations'),
};
