import * as R from 'ramda';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { withFormik } from 'formik';
import { goBack } from 'react-router-redux';
import { apiHelper } from 'app/services/api/api-helper';
import { withDetail } from 'app/hoc/detail';

export const withEditDetail = ({ fields, apiName }) =>
  compose(
    connect(
      null,
      dispatch => ({ goBack: () => dispatch(goBack()) })
    ),
    withDetail(apiHelper(apiName)),
    withProps(() => ({
      fields,
    })),
    withFormik({
      mapPropsToValues: ({ detail: { data, loaded } }) =>
        loaded
          ? // if loaded return data
            R.pick(R.pluck('name')(fields), data)
          : // else return data with empty fields
            R.reduce(
              (acc, field) => R.assoc(field, '', acc),
              {},
              R.pluck('name')(fields)
            ),
      validate: null,
      handleSubmit: (values, { props, setSubmitting }) => {
        const { update, detail, goBack } = props as any;
        update({ id: detail.data.id, values }).then(() => {
          setSubmitting(false);
          goBack();
        });
      },
      enableReinitialize: true,
    })
  );
