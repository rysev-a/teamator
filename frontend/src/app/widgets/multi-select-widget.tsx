import * as React from 'react';
import { FieldArray } from 'formik';

import { MultiSelect } from 'app/components/ui';
import { withMultiSelect } from 'app/hoc/multi-select';

export const getMultiSelectWidget = apiName => {
  const Select = withMultiSelect({
    label: 'label',
    apiName,
  })(MultiSelect);

  return ({ values }) => (
    <FieldArray
      name={apiName}
      render={arrayHelpers => {
        return (
          <Select
            label="tags"
            apiName={apiName}
            selected={values.tags}
            reset={arrayHelpers.remove}
            insert={arrayHelpers.insert}
          />
        );
      }}
    />
  );
};
