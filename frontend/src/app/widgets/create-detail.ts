import * as R from 'ramda';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { withFormik } from 'formik';
import { push } from 'react-router-redux';
import { apiHelper } from 'app/services/api/api-helper';
import { withDetail } from 'app/hoc/detail';

export const withCreateDetail = ({ fields, apiName }) =>
  compose(
    connect(
      null,
      dispatch => ({ goBack: () => dispatch(push(`/admin/${apiName}`)) })
    ),
    withDetail(apiHelper(apiName)),
    withProps(() => ({
      fields,
    })),
    withFormik({
      mapPropsToValues: ({ detail: { data, loaded } }) =>
        R.reduce(
          (acc, field) => R.assoc(field, '', acc),
          {},
          R.pluck('name')(fields)
        ),
      validate: null,
      handleSubmit: (values, { props, setSubmitting }) => {
        const { create, detail, goBack } = props as any;
        create({ values }).then(() => {
          setSubmitting(false);
          goBack();
        });
      },
      enableReinitialize: true,
    })
  );
