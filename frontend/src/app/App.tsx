import * as React from 'react';
import { Header } from 'app/components/common';
import { AccountWrapperContainer } from 'app/modules/account';

import { Route, Switch } from 'react-router-dom';
import {
  SigninPage,
  SignupPage,
  ErrorPage,
  StartPage,
  ProfileSettingsPage,
  ProfileProjectsPage,
  ProfileInvitationsPage,
  UserListPage,
  UserDetailPage,
  ProjectListPage,
  ProjectDetailPage,
  ProjectCreatePage,
  ProjectEditPage,
} from 'app/pages';

import Admin from 'app/admin';

const App = () => (
  <main>
    <Header />
    <AccountWrapperContainer>
      <Switch>
        <Route path="/" exact component={StartPage} />
        <Route path="/users" exact component={UserListPage} />
        <Route path="/users/:id" exact component={UserDetailPage} />
        <Route path="/projects" exact component={ProjectListPage} />
        <Route path="/projects/create" exact component={ProjectCreatePage} />
        <Route path="/projects/:id" exact component={ProjectDetailPage} />
        <Route path="/projects/:id/edit" exact component={ProjectEditPage} />
        <Route path="/signin" component={SigninPage} />
        <Route path="/signup" component={SignupPage} />
        <Route path="/profile/settings" component={ProfileSettingsPage} />
        <Route path="/profile/projects" component={ProfileProjectsPage} />
        <Route path="/profile/invitations" component={ProfileInvitationsPage} />
        <Route path="/admin" component={Admin} />
        <Route component={ErrorPage} />
      </Switch>
    </AccountWrapperContainer>
  </main>
);

export default App;
