import * as React from 'react';
import { ProjectListContainer } from 'app/modules/projects';

export const ProjectListPage = () => (
  <div className="section">
    <div className="container">
      <div className="user-list-page">
        <ProjectListContainer />
      </div>
    </div>
  </div>
);
