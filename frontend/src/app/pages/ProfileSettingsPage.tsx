import * as React from 'react';
import { ProfileSettingsContainer } from 'app/modules/profile';

export const ProfileSettingsPage = () => (
  <div className="section">
    <div className="container">
      <div className="profile-settings-page">
        <ProfileSettingsContainer />
      </div>
    </div>
  </div>
);
