import * as React from 'react';
import { UserListContainer } from 'app/modules/users';

export const UserListPage = () => (
  <div className="section">
    <div className="container">
      <div className="user-list-page">
        <UserListContainer />
      </div>
    </div>
  </div>
);
