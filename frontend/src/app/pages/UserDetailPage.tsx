import * as React from 'react';
import { UserDetailContainer } from 'app/modules/users';

export const UserDetailPage = () => (
  <div className="user-detail-page">
    <UserDetailContainer />
  </div>
);
