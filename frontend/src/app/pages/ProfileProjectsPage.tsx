import * as React from 'react';
import { ProfileProjectsContainer } from 'app/modules/profile';

export const ProfileProjectsPage = () => (
  <div className="section">
    <div className="container">
      <div className="profile-projects-page">
        <ProfileProjectsContainer />
      </div>
    </div>
  </div>
);
