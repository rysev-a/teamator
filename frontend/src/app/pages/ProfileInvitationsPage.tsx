import * as React from 'react';
import { ProfileInvitationsContainer } from 'app/modules/profile';

export const ProfileInvitationsPage = () => (
  <div className="section">
    <div className="container">
      <div className="profile-invitations-page">
        <ProfileInvitationsContainer />
      </div>
    </div>
  </div>
);
