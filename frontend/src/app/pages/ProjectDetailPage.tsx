import * as React from 'react';
import { ProjectDetailContainer } from 'app/modules/projects';

export const ProjectDetailPage = () => (
  <div className="projects-detail-page">
    <ProjectDetailContainer />
  </div>
);
