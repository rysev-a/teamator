import * as React from 'react';
import { ProjectEditContainer } from 'app/modules/projects';

export const ProjectEditPage = () => (
  <div className="projects-edit-page">
    <ProjectEditContainer />
  </div>
);
