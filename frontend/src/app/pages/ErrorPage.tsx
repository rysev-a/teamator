import * as React from 'react';

const ErrorPage = () => (
  <div className="section">
    <div className="container">
      <h1 className="is-size-1  title has-text-weight-normal">Ошибка 404</h1>
      <p className="is-size-4">Такой страницы не существует</p>
    </div>
  </div>
);

export default ErrorPage;
