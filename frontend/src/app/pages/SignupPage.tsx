import * as React from 'react';

import Signup from 'app/modules/signup';

const SignupPage = () => (
  <div className="signup-page">
    <Signup />
  </div>
);

export default SignupPage;
