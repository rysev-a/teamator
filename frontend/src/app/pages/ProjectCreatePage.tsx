import * as React from 'react';
import { ProjectCreateContainer } from 'app/modules/projects';

export const ProjectCreatePage = () => (
  <div className="projects-create-page">
    <ProjectCreateContainer />
  </div>
);
