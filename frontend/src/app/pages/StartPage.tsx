import * as React from 'react';

const ErrorPage = () => (
  <div className="section">
    <div className="container">
      <h1 className="is-size-1  title has-text-weight-normal">
        Главная страница
      </h1>
      <p className="is-size-4">Добро пожаловать на главную страницу</p>
    </div>
  </div>
);

export default ErrorPage;
