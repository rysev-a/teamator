import * as React from 'react';

import Signin from 'app/modules/signin';

const SigninPage = () => (
  <div className="signin-page">
    <Signin />
  </div>
);

export default SigninPage;
