import * as React from 'react';
import { render } from 'react-dom';
import { ConnectedRouter } from 'react-router-redux';
import { store, history } from './core/store';
import { Provider } from 'react-redux';
import App from './App';
import 'bulma/css/bulma.min.css';

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.querySelector('#app')
);
