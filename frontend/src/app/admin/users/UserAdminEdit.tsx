import { withEditDetail } from 'app/widgets/edit-detail';
import { Detail } from 'app/components/ui';
import { userFields } from '../fields.yml';

export default withEditDetail({ fields: userFields, apiName: 'users' })(Detail);
