import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import UserAdminList from './UserAdminList';
import UserAdminEdit from './UserAdminEdit';
import UserAdminCreate from './UserAdminCreate';

const Users = () => (
  <div className="users">
    <Switch>
      <Route path="/admin/users" exact component={UserAdminList} />
      <Route path="/admin/users/create" component={UserAdminCreate} />
      <Route path="/admin/users/:id" component={UserAdminEdit} />
    </Switch>
  </div>
);

export default Users;
