import { withCreateDetail } from 'app/widgets/create-detail';
import { Detail } from 'app/components/ui';
import { userFields } from '../fields.yml';

export default withCreateDetail({ fields: userFields, apiName: 'users' })(
  Detail
);
