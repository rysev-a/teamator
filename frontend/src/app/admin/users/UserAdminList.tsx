import { compose, withProps } from 'recompose';
import { withList } from 'app/hoc/list';
import { Table } from 'app/components/ui';
import { userApi } from 'app/services/api';
import { userFields, userTable } from '../fields.yml';

const UserAdminList = compose(
  withList(userApi, {
    sorting: {
      key: 'id',
      order: 'asc',
    },
  }),
  withProps(() => ({
    table: userTable,
  }))
)(Table);

export default UserAdminList;
