import * as React from 'react';
import { NavLink } from 'react-router-dom';

const AdminMenu = () => (
  <aside className="menu">
    <p className="menu-label">Cервисы</p>
    <ul className="menu-list">
      <li>
        <NavLink activeClassName="is-active" exact to="/admin/users">
          Пользователи
        </NavLink>
      </li>
      <li>
        <NavLink activeClassName="is-active" exact to="/admin/projects">
          Проекты
        </NavLink>
      </li>
      <li>
        <NavLink activeClassName="is-active" exact to="/admin/invitations">
          Приглашения
        </NavLink>
      </li>
    </ul>
    <p className="menu-label">Настройки</p>
    <ul className="menu-list">
      <li>
        <a>Уведомления</a>
      </li>
      <li>
        <NavLink to="/admin/forms">Формы</NavLink>
        <ul>
          <li>
            <a>Фильтры</a>
          </li>
          <li>
            <a>Поля оформления</a>
          </li>
          <li>
            <a>Типы полей</a>
          </li>
        </ul>
      </li>
    </ul>
  </aside>
);

export default AdminMenu;
