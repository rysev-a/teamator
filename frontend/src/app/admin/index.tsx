import * as React from 'react';
import { Route } from 'react-router-dom';
import Users from './users';
import Projects from './projects';
import Invitations from './invitations';
import AdminMenu from './AdminMenu';

const Admin = () => (
  <section className="section">
    <div className="container">
      <div className="columns">
        <div className="column is-one-quarter">
          <AdminMenu />
        </div>
        <div className="column">
          <Route path="/admin/users" component={Users} />
          <Route path="/admin/invitations" component={Invitations} />
          <Route path="/admin/projects" component={Projects} />
        </div>
      </div>
    </div>
  </section>
);

export default Admin;
