import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { compose, withProps } from 'recompose';
import { withList } from 'app/hoc/list';
import { withEditDetail } from 'app/widgets/edit-detail';
import { withCreateDetail } from 'app/widgets/create-detail';
import { Table } from 'app/components/ui';
import { invitationApi } from 'app/services/api';
import { Detail } from 'app/components/ui';
import { invitationFields, invitationTable } from '../fields.yml';

const InvitationAdminList = compose(
  withList(invitationApi, {
    sorting: {
      key: 'id',
      order: 'asc',
    },
  }),
  withProps(() => ({
    table: invitationTable,
  }))
)(Table);

const InvitationAdminEdit = withEditDetail({
  fields: invitationFields,
  apiName: 'invitations',
})(Detail);

const InvitationAdminCreate = withCreateDetail({
  fields: invitationFields,
  apiName: 'invitations',
})(Detail);

const Invitations = () => (
  <div className="invitations">
    <Switch>
      <Route path="/admin/invitations" exact component={InvitationAdminList} />
      <Route
        path="/admin/invitations/create"
        component={InvitationAdminCreate}
      />
      <Route path="/admin/invitations/:id" component={InvitationAdminEdit} />
    </Switch>
  </div>
);

export default Invitations;
