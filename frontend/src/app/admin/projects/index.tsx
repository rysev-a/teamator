import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { compose, withProps } from 'recompose';
import { withList } from 'app/hoc/list';
import { withEditDetail } from 'app/widgets/edit-detail';
import { withCreateDetail } from 'app/widgets/create-detail';
import { Table } from 'app/components/ui';
import { projectApi } from 'app/services/api';
import { Detail } from 'app/components/ui';
import { projectFields, projectTable } from '../fields.yml';

const ProjectAdminList = compose(
  withList(projectApi, {
    sorting: {
      key: 'id',
      order: 'asc',
    },
  }),
  withProps(() => ({
    table: projectTable,
  }))
)(Table);

const ProjectAdminEdit = withEditDetail({
  fields: projectFields,
  apiName: 'projects',
})(Detail);

const ProjectAdminCreate = withCreateDetail({
  fields: projectFields,
  apiName: 'projects',
})(Detail);

const Projects = () => (
  <div className="projects">
    <Switch>
      <Route path="/admin/projects" exact component={ProjectAdminList} />
      <Route path="/admin/projects/create" component={ProjectAdminCreate} />
      <Route path="/admin/projects/:id" component={ProjectAdminEdit} />
    </Switch>
  </div>
);

export default Projects;
