import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import accountReducer from 'app/modules/account/reducers';

export default combineReducers({
  router: routerReducer,
  account: accountReducer,
});
