import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import thunk from 'redux-thunk';
import reducers from './reducers';

// only for development
import { composeWithDevTools } from 'redux-devtools-extension';

declare global {
  interface Window {
    hot: any;
  }
}

// only for development
window.hot = window.hot || {};

export const configHistory = () => {
  if (window.hot.history) {
    return window.hot.history;
  }

  const windowHistory = createHistory();
  window.hot.history = windowHistory;

  return window.hot.history;
};

export const configStore = history => {
  if (window.hot.store) {
    return window.hot.store;
  }

  const windowStore = createStore(
    reducers,
    composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history)))
  );

  window.hot.store = windowStore;
  return window.hot.store;
};

export const history = configHistory();
export const store = configStore(history);
