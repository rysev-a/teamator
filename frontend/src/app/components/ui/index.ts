import MultiSelect from './MultiSelect';
import Processing from './Processing';
import Pagination from './Pagination';
import Select from './Select';
import Table from './Table';
import Icon from './Icon';
import Detail from './Detail';
import Tag from './Tag';

export {
  Processing,
  Icon,
  Pagination,
  MultiSelect,
  Table,
  Select,
  Detail,
  Tag,
};
