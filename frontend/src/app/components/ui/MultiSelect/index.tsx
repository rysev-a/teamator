import * as React from 'react';
import * as R from 'ramda';
import classNames from 'classnames';
import './MultiSelect.css';

const MultiSelect = ({
  selected,
  insert,
  reset,
  list: { data, status },
  updateFilter,
  multiSelect: { isOpen, label },
  toggle,
}) => (
  <div className="multi-select field">
    <label className="label">{label}</label>
    <div className="multi-selected">
      <div className="tags">
        {selected.map(({ id, title }, index) => (
          <span className="tag is-medium is-primary" key={id}>
            {title}
            <button className="delete" onClick={reset.bind(null, index)} />
          </span>
        ))}
      </div>
    </div>
    <div
      className={classNames('select', {
        'is-loading': status.processing,
      })}>
      <input
        className="input"
        onClick={toggle}
        onChange={e => updateFilter({ id: 'title', value: e.target.value })}
      />
    </div>
    <div
      className={classNames('multi-select-options', {
        'is-open': isOpen,
      })}>
      {R.without(selected)(R.map(({ id, title }) => ({ id, title }))(data)).map(
        ({ id, title }) => (
          <div
            key={id}
            value={id}
            onClick={() => {
              insert(selected.length, { id, title });
            }}>
            <span className="tag is-medium multi-select-options__item">
              {title}
            </span>
          </div>
        )
      )}
    </div>
  </div>
);

export default MultiSelect;
