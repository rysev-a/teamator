import * as React from 'react';

const Select = ({ onChange, options, value }) => (
  <div className="select">
    <select value={value} onChange={onChange}>
      {options.map(({ id, title }) => (
        <option key={id} value={id}>
          {title}
        </option>
      ))}
    </select>
  </div>
);

export default Select;
