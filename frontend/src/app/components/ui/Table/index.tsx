import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { Pagination } from 'app/components/ui';
import { Processing, Icon } from 'app/components/ui';

const Table = ({
  table: { title, fields, uniqKey, editUrl },
  list: { data, pagination, status, filter },
  pageSet,
  remove,
}) => (
  <div className="table-wrapper">
    <Processing processing={status.processing} />
    <h3 className="table-title is-size-4 title has-text-weight-normal">
      {title}
    </h3>
    <table className="is-fullwidth table">
      <thead>
        <tr>
          {fields.map(({ title, name }) => (
            <th key={name}>{title}</th>
          ))}
          <th>actions</th>
        </tr>
      </thead>
      <tbody>
        {data.map(detail => (
          <tr key={detail[uniqKey]}>
            {fields.map(({ name, type, key }) => (
              <td key={name}>
                {type === 'object' ? detail[name][key] : detail[name]}
              </td>
            ))}
            <td>
              <NavLink to={`/${editUrl}/${detail[uniqKey]}`}>
                <Icon type="pencil-alt" color="dark" />
              </NavLink>

              <a onClick={remove.bind(null, detail[uniqKey])}>
                <Icon type="trash" color="danger" />
              </a>
            </td>
          </tr>
        ))}
      </tbody>
      <tfoot>
        <tr>
          <td colSpan={fields.length + 1}>
            <Pagination pagination={pagination} pageSet={pageSet} />
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
);

export default Table;
