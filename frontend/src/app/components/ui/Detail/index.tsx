import * as React from 'react';
import { Processing } from 'app/components/ui';

const Field = ({ name, onChange, value, type, placeholder }) => {
  if (type === 'textarea') {
    return (
      <textarea
        onChange={onChange}
        name={name}
        value={value}
        placeholder={placeholder}
        className="textarea"
      />
    );
  } else {
    return (
      <input
        onChange={onChange}
        name={name}
        value={value}
        placeholder={placeholder}
        className="input"
      />
    );
  }
};

const Detail = ({
  detail: { processing },
  values,
  handleChange,
  handleSubmit,
  isSubmitting,
  fields,
}) => (
  <div className="detail columns">
    <Processing processing={processing || isSubmitting} />
    <form className="detail-form column is-half" onSubmit={handleSubmit}>
      {fields.map(({ title, name, type }) => (
        <div className="field" key={name}>
          <label className="label">{title}</label>
          <div className="control">
            <Field
              name={name}
              onChange={handleChange}
              value={values[name]}
              type={type}
              placeholder={name}
            />
          </div>
        </div>
      ))}
      <button type="submit" className="button">
        Сохранить
      </button>
    </form>
  </div>
);

export default Detail;
