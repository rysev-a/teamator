import * as React from 'react';
import classNames from 'classnames';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// import only if need icon
import {
  faPencilAlt,
  faTrash,
  faCloud,
  faPlus,
} from '@fortawesome/free-solid-svg-icons';

library.add(faPencilAlt, faTrash, faCloud, faPlus);

const Icon = ({ type, color, size }: any) => (
  <span
    className={classNames('icon', {
      [`is-${size}`]: size,
    })}>
    <FontAwesomeIcon
      icon={type}
      className={classNames({
        [`has-text-${color}`]: color,
        [`fa-lg`]: size === 'large',
      })}
    />
  </span>
);

export default Icon;
