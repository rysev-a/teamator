import * as React from 'react';
import classNames from 'classnames';

interface TagSettings {
  color?: string;
  size?: string;
  children?: object;
}

const Tag = ({ color, size, children }: TagSettings) => (
  <span
    className={classNames('tag', {
      [`is-${color}`]: color,
    })}>
    {children}
  </span>
);

export default Tag;
