import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { style } from 'typestyle';
import classNames from 'classnames';
import { AccountMenuContainer, HeaderMenuContainer } from 'app/modules/account';

export const Header = () => (
  <header
    className={style({
      borderBottom: '1px solid #e6eaee',
    })}>
    <div className="container">
      <nav className="navbar">
        <NavLink
          to="/"
          className={classNames(
            'navbar-brand',
            'is-size-4',
            style({
              display: 'flex',
              alignItems: 'center',
            })
          )}>
          Teamator
        </NavLink>
        <HeaderMenuContainer />
        <AccountMenuContainer />
      </nav>
    </div>
  </header>
);
