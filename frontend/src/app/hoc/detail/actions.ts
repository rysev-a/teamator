import constants from './constants';

const detailActions = {
  fetchStart: () => ({
    type: constants.FETCH_START,
  }),

  fetchSuccess: payload => ({
    type: constants.FETCH_SUCCESS,
    payload,
  }),

  fetchError: payload => ({
    type: constants.FETCH_ERROR,
    payload,
  }),

  updateStart: () => ({
    type: constants.UPDATE_START,
  }),

  updateSuccess: payload => ({
    type: constants.UPDATE_SUCCESS,
    payload,
  }),

  updateError: payload => ({
    type: constants.UPDATE_ERROR,
    payload,
  }),

  createStart: () => ({
    type: constants.CREATE_START,
  }),

  createSuccess: payload => ({
    type: constants.CREATE_SUCCESS,
    payload,
  }),

  createError: payload => ({
    type: constants.CREATE_ERROR,
    payload,
  }),
};

export default detailActions;
