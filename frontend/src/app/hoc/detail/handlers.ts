import actions from './actions';

const createHandlers = api => {
  const handlers = {
    load: ({
      localDispatch,
      match: {
        params: { id },
      },
    }) => () => {
      // load user if id is exist (or open create form)
      if (id) {
        localDispatch(actions.fetchStart());
        return api.detail
          .get(id)
          .then(({ data }) => localDispatch(actions.fetchSuccess(data)))
          .catch(error => localDispatch(actions.fetchError(error)));
      }
    },

    update: ({ localDispatch }) => ({ id, values }) =>
      api.detail
        .put({ id, values })
        .then(({ data }) => localDispatch(actions.updateSuccess(data))),

    create: ({ localDispatch }) => ({ values }) =>
      api.list
        .post(values)
        .then(({ data }) => localDispatch(actions.createSuccess(data))),
  };

  return handlers;
};

export default createHandlers;
