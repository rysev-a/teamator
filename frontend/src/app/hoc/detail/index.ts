import detailReducer from './reducer';
import createHandlers from './handlers';
import { withRouter } from 'react-router';
import { compose, withReducer, withHandlers, lifecycle } from 'recompose';

const withDetail = (api, initialState?) =>
  compose(
    withRouter,
    withReducer('detail', 'localDispatch', detailReducer, initialState),
    withHandlers(createHandlers(api)),
    lifecycle({
      componentDidMount() {
        this.props.load(this.props);
      },
    })
  );

export { detailReducer, createHandlers, withDetail };
