import constants from './constants';

const defaultState = () => ({ isOpen: false, label: '' });

const multiSelectReducer = (state = defaultState(), action) => {
  if (action.type === constants.OPEN) {
    return { ...state, isOpen: true };
  }

  if (action.type === constants.CLOSE) {
    return { ...state, isOpen: false };
  }

  if (action.type === constants.TOGGLE) {
    return { ...state, isOpen: !state.isOpen };
  }

  return state;
};

export default multiSelectReducer;
