import constants from './constants';

const multiSelectHandlers = {
  open: ({ dispatch }) => () => dispatch({ type: constants.OPEN }),
  close: ({ dispatch }) => () => dispatch({ type: constants.CLOSE }),
  toggle: ({ dispatch }) => () => dispatch({ type: constants.TOGGLE }),
};

export default multiSelectHandlers;
