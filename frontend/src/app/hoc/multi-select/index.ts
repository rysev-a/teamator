import { compose, withReducer, withHandlers } from 'recompose';
import { apiHelper } from 'app/services/api/api-helper';
import multiselectReducer from './reducer';
import multiselectHandlers from './handlers';
import { withList } from 'app/hoc/list';

export const withMultiSelect = ({ label, apiName }) =>
  compose(
    withList(apiHelper(apiName), {
      pagination: {
        page: 1,
        pages: 0,
        count: 50,
      },
      filters: [
        {
          id: 'title',
          key: 'title',
          value: '',
          operator: 'startWith',
        },
      ],
      data: [],
      status: {},
    }),
    withReducer('multiSelect', 'dispatch', multiselectReducer, { label }),
    withHandlers(multiselectHandlers)
  );
