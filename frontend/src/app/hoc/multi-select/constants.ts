export default {
  OPEN: 'open',
  CLOSE: 'close',
  TOGGLE: 'toggle',
};
