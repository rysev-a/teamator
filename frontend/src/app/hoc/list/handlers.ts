import actions from './actions';

const createHandlers = api => {
  const handlers = {
    load: ({ localDispatch, list }) => () => {
      localDispatch(actions.fetchStart());
      return api.list
        .get(list)
        .then(({ data }) => localDispatch(actions.fetchSuccess(data)));
    },

    pageSet: ({ localDispatch, list }) => page =>
      localDispatch(actions.setPage(page), list =>
        handlers.load({ localDispatch, list })()
      ),

    updateFilter: ({ localDispatch, list }) => ({ id, value }) =>
      // reset pagination to first page
      localDispatch(actions.setPage(1), () =>
        //update filters
        localDispatch(actions.updateFilters({ id, value }), list => {
          // load new data after filter change
          handlers.load({ localDispatch, list })();
        })
      ),

    initFilters: ({ localDispatch }) => filters =>
      localDispatch(actions.initFilters(filters)),

    remove: ({ localDispatch, list }) => id => {
      localDispatch(actions.removeStart());
      api.list
        .delete(id)
        .then(handlers.load({ localDispatch, list }))
        .catch(() => localDispatch(actions.removeError()));
    },
  };

  return handlers;
};

export default createHandlers;
