export default {
  FETCH_START: 'fetch start',
  FETCH_SUCCESS: 'fetch success',
  FETCH_ERROR: 'fetch error',

  REMOVE_START: 'remove start',
  REMOVE_SUCCESS: 'remove success',
  REMOVE_ERROR: 'remove error',

  UPDATE_PAGINATION: 'update pagination',
  SET_PAGE: 'update page',

  INIT_FILTERS: 'init filters',
  UPDATE_FILTERS: 'update filters',

  UPDATE_SORTING: 'update sorting',

  RESET: 'reset',
};
