import listReducer from './reducer';
import createHandlers from './handlers';
import { compose, withReducer, withHandlers, lifecycle } from 'recompose';

const withList = (api, initialState?) =>
  compose(
    withReducer('list', 'localDispatch', listReducer, {
      ...listReducer(undefined, { type: 'init' }),
      ...initialState,
    }),
    withHandlers(createHandlers(api)),
    lifecycle({
      componentDidMount() {
        this.props.load(this.props);
      },
    })
  );

export { listReducer, createHandlers, withList };
