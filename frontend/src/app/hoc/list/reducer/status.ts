import constants from '../constants';

const defaultState = () => ({
  loaded: false,
  processing: false,
});

const statusReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.FETCH_START:
    case constants.REMOVE_START:
      return {
        ...state,
        processing: true,
      };

    case constants.FETCH_ERROR:
    case constants.REMOVE_ERROR:
      return {
        ...state,
        processing: false,
      };

    case constants.FETCH_SUCCESS:
      return {
        processing: false,
        loaded: true,
      };

    case constants.RESET:
      return defaultState();

    default:
      return state;
  }
};

export default statusReducer;
