import { combineReducers } from 'redux';
import data from './data';
import pagination from './pagination';
import status from './status';
import filters from './filters';
import sorting from './sorting';

export default combineReducers({ data, pagination, status, filters, sorting });
