import constants from '../constants';

const defaultState = () => ({});

const sortingReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.UPDATE_SORTING:
      return action.payload;

    default:
      return state;
  }
};

export default sortingReducer;
