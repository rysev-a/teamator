import constants from '../constants';

const defaultState = () => ({
  page: 1,
  pages: 0,
  count: 10,
});

const paginationReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.FETCH_SUCCESS:
      return {
        ...state,
        pages: action.payload.pages,
        page: action.payload.page,
      };

    case constants.SET_PAGE:
      return {
        ...state,
        page: action.payload,
      };

    case constants.RESET:
      return defaultState();

    default:
      return state;
  }
};

export default paginationReducer;
