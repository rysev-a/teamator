import constants from '../constants';
import * as R from 'ramda';

const defaultState = () => [];

const filtersReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case constants.INIT_FILTERS:
      return action.payload;

    case constants.UPDATE_FILTERS: {
      const { id, value } = action.payload;
      const index = R.findIndex(R.propEq('id', id))(state);
      return R.update(index)({ ...state[index], value })(state);
    }

    case constants.RESET:
      return defaultState();

    default:
      return state;
  }
};

export default filtersReducer;
