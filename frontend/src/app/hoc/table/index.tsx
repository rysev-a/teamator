import { withProps } from 'recompose';

export const withTable = ({ title, fields, uniqKey, editUrl }) =>
  withProps({ table: { title, fields, uniqKey, editUrl } });
