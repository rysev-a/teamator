import constants from './constants';
import { accountApi } from 'app/services/api';
import { push } from 'react-router-redux';

const accountActions = {
  load: () => dispatch => {
    dispatch(accountActions.fetchStart());

    return accountApi
      .load()
      .then(({ data }) => dispatch(accountActions.fetchSuccess(data)))
      .catch(() => dispatch(accountActions.fetchError()));
  },

  fetchStart: () => ({
    type: constants.ACCOUNT_FETCH_START,
  }),

  fetchSuccess: data => ({
    type: constants.ACCOUNT_FETCH_SUCCESS,
    payload: data,
  }),

  fetchError: () => ({
    type: constants.ACCOUNT_FETCH_ERROR,
  }),

  reset: () => ({
    type: constants.ACCOUNT_RESET,
  }),

  redirect: ({ account, router }) => dispatch => {
    const path = router.location.pathname;

    if (
      account.isAuth &&
      account.loaded &&
      ['/signin', '/signup'].indexOf(path) != -1
    ) {
      dispatch(push('/'));
    }

    if (account.loaded && !account.isAuth && path == '/profile') {
      dispatch(push('/'));
    }
  },

  signout: () => dispatch => {
    accountApi.signout().then(() => dispatch(accountActions.load()));
  },
};

export default accountActions;
