export default {
  ACCOUNT_FETCH_START: 'account fetch start',
  ACCOUNT_FETCH_SUCCESS: 'account fetch success',
  ACCOUNT_FETCH_ERROR: 'account fetch error',
  ACCOUNT_RESET: 'account reset',
};
