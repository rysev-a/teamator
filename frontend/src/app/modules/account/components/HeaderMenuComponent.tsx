import * as React from 'react';
import { style } from 'typestyle';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

const HeaderMenuComponent = ({ account }) =>
  account.loaded && (
    <div
      className={classNames(
        style({
          marginLeft: '30px',
        }),
        'navbar-menu'
      )}>
      <div
        className={classNames('navbar-item', {
          'has-dropdown is-hoverable': account.isAuth,
        })}
        id="project-dropdown">
        <NavLink
          to="/projects"
          className={account.isAuth ? 'navbar-link' : 'navbar-item'}>
          Проекты
        </NavLink>
        {account.isAuth && (
          <div className="navbar-dropdown">
            <div className="sign-menu">
              <NavLink to="/projects/create" className="navbar-item">
                Создать проект
              </NavLink>
            </div>
          </div>
        )}
      </div>

      <NavLink to="/users" className="navbar-item">
        Пользователи
      </NavLink>
    </div>
  );

export default HeaderMenuComponent;
