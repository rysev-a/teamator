import * as React from 'react';
import { Processing } from 'app/components/ui';
import { style } from 'typestyle';

const AccountWrapperComponent = ({ account, children }) => (
  <main
    className={style({
      position: 'relative',
    })}>
    <Processing processing={!account.loaded} />
    {account.loaded && children}
  </main>
);

export default AccountWrapperComponent;
