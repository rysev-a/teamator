import * as React from 'react';
import { NavLink } from 'react-router-dom';

const AccountMenu = ({ account, signout }) =>
  account.loaded && (
    <div className="navbar-end">
      <div className="navbar-menu">
        {account.isAuth && (
          <div
            className="navbar-item has-dropdown is-hoverable"
            id="account-dropdown">
            <a className="navbar-link">{account.data.email}</a>
            <div className="navbar-dropdown">
              {account.data.role.name === 'admin' && (
                <NavLink to="/admin" className="navbar-item">
                  Административная панель
                </NavLink>
              )}
              <NavLink className="navbar-item" to="/profile/projects">
                Мои проекты
              </NavLink>
              <NavLink className="navbar-item" to="/profile/invitations">
                Приглашения
              </NavLink>
              <NavLink className="navbar-item" to="/profile/settings">
                Настройки
              </NavLink>
              <div className="sign-menu">
                <a className="navbar-item" onClick={signout}>
                  Выйти
                </a>
              </div>
            </div>
          </div>
        )}
        {!account.isAuth && (
          <div className="navbar-menu">
            <NavLink className="navbar-item" to="/signin">
              Вход
            </NavLink>
            <NavLink className="navbar-item" to="/signup">
              Регистрация
            </NavLink>
          </div>
        )}
      </div>
    </div>
  );

export default AccountMenu;
