import { connect } from 'react-redux';
import AccountWrapperComponent from '../components/AccountWrapperComponent';

export const AccountWrapperContainer = connect(({ account }) => ({ account }))(
  AccountWrapperComponent
);
