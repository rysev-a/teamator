import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import actions from '../actions';
import { bindActionCreators } from 'redux';
import AccountMenuComponent from '../components/AccountMenuComponent';

export const AccountMenuContainer = compose(
  connect(
    ({ account, router }) => ({ account, router }),
    dispatch => bindActionCreators(actions, dispatch)
  ),
  lifecycle({
    componentDidMount() {
      this.props.load();
      this.props.redirect(this.props);
    },
    componentDidUpdate() {
      this.props.redirect(this.props);
    },
  })
)(AccountMenuComponent);
