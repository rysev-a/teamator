import { connect } from 'react-redux';
import HeaderMenuComponent from '../components/HeaderMenuComponent';

export const HeaderMenuContainer = connect(({ account }) => ({ account }))(
  HeaderMenuComponent
);
