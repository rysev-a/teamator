import ProjectListComponent from '../components/ProjectListComponent';
import { projectApi } from 'app/services/api';
import { withList } from 'app/hoc/list';

import { compose } from 'recompose';

export const ProjectListContainer = compose(
  withList(projectApi, {
    pagination: { count: 15 },
  })
)(ProjectListComponent);
