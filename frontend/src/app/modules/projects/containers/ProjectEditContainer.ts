import * as R from 'ramda';
import { connect } from 'react-redux';
import { compose, withHandlers, withProps } from 'recompose';
import { projectApi } from 'app/services/api';
import { withDetail } from 'app/hoc/detail';
import { withFormik } from 'formik';
import { push } from 'react-router-redux';
import ProjectEditComponent from '../components/ProjectEditComponent';
import projectHandlers from '../handers';

export const ProjectEditContainer = compose(
  connect(
    null,
    dispatch => ({
      // timeout for formik unmount state
      goBack: () => setTimeout(() => dispatch(push('/profile/projects')), 100),
    })
  ),
  withDetail(projectApi),
  // exclude invitations with accomplish status
  withProps(({ detail: { data } }) => ({
    invitationExcludes: R.compose(
      R.map(invitation => ({
        value: invitation.receiving_user.id,
        label: invitation.receiving_user.email,
      })),
      R.filter(invitation => invitation.status != 'pending')
    )(data.invitations || []),
  })),
  withHandlers(projectHandlers),
  withFormik({
    mapPropsToValues: ({ detail: { data } }) => ({
      name: data.name || '',
      description: data.description || '',
      members: R.compose(
        R.map(invitation => ({
          value: invitation.receiving_user.id,
          label: invitation.receiving_user.email,
        })),
        R.filter(invitation => invitation.status == 'success')
      )(data.invitations || []),
      invite_users: R.compose(
        R.map(invitation => ({
          value: invitation.receiving_user.id,
          label: invitation.receiving_user.email,
        })),
        R.filter(invitation => invitation.status == 'pending')
      )(data.invitations || []),
    }),
    validate: null,
    handleSubmit: (values, { props }) => {
      const { updateProject, goBack, detail } = props as any;
      const {
        data: { id },
      } = detail;
      return updateProject({ id, values }).then(goBack);
    },
    enableReinitialize: true,
  })
)(ProjectEditComponent);
