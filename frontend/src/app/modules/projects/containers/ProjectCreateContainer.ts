import { connect } from 'react-redux';
import { compose, withHandlers } from 'recompose';
import { withFormik } from 'formik';
import { push } from 'react-router-redux';
import ProjectCreateComponent from '../components/ProjectCreateComponent';
import projectHandlers from '../handers';

export const ProjectCreateContainer = compose(
  connect(
    null,
    dispatch => ({
      // timeout for formik unmount state
      goBack: () => setTimeout(() => dispatch(push('/profile/projects')), 100),
    })
  ),
  withHandlers(projectHandlers),
  withFormik({
    mapPropsToValues: () => ({
      name: '',
      description: '',
      invite_users: [],
    }),
    validate: null,
    handleSubmit: (values, { props }) => {
      const { createProject, goBack } = props as any;
      return createProject(values).then(goBack);
    },
    enableReinitialize: true,
  })
)(ProjectCreateComponent);
