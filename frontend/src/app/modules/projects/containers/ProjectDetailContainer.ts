import { compose } from 'recompose';
import { withDetail } from 'app/hoc/detail';
import { projectApi } from 'app/services/api';
import ProjectDetailComponent from '../components/ProjectDetailComponent';

export const ProjectDetailContainer = compose(withDetail(projectApi))(
  ProjectDetailComponent
);
