import { projectApi } from 'app/services/api';

const projectHandlers = {
  createProject: () => values => projectApi.create.post(values),
  updateProject: () => ({ id, values }) =>
    projectApi.update.put({ id, values }),
};

export default projectHandlers;
