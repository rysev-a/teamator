import * as R from 'ramda';
import * as React from 'react';
import { NavLink } from 'react-router-dom';
import { Processing } from 'app/components/ui';

const ProjectDetailComponent = ({
  detail: {
    data: { id, name, description, invitations, creator },
    loaded,
    processing,
  },
}) => (
  <div className="users">
    <Processing processing={processing} />

    <section className="hero is-dark">
      <div className="hero-body">
        <div className="container">
          {loaded && (
            <div className="container">
              <h1 className="is-size-1  title has-text-weight-normal">
                #{id} {name}
              </h1>
            </div>
          )}
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        {loaded && (
          <div className="content">
            <h2 className="is-size-4 title has-text-weight-normal">
              Основатель
            </h2>
            <p>
              <span className="is-size-5">
                {creator.first_name} {creator.last_name}
              </span>
            </p>
            <h2 className="is-size-4 title has-text-weight-normal">Описание</h2>
            <p>{description}</p>
            <h2 className="is-size-4 title has-text-weight-normal">
              Участники
            </h2>
            {R.filter(invitation => invitation.status == 'success')(invitations)
              .length > 0 ? (
              <table>
                <thead>
                  <tr>
                    <th>Id</th>
                    <td>Электронная почта</td>
                    <td>Имя</td>
                    <td>Фамилия</td>
                  </tr>
                </thead>
                <tbody>
                  {R.compose(
                    R.map(
                      ({
                        receiving_user: { id, first_name, last_name, email },
                      }) => (
                        <tr key={id}>
                          <td>{id}</td>
                          <td>
                            <NavLink to={`/users/${id}`}>{email}</NavLink>
                          </td>
                          <td>{first_name}</td>
                          <td>{last_name}</td>
                        </tr>
                      )
                    ),
                    R.filter(invitation => invitation.status == 'success')
                  )(invitations)}
                </tbody>
              </table>
            ) : (
              <h3 className="is-size-5 title has-text-weight-normal">
                Пока что нет ни одного участника
              </h3>
            )}
          </div>
        )}
      </div>
    </section>
  </div>
);

export default ProjectDetailComponent;
