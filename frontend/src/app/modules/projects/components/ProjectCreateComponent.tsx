import * as React from 'react';
import { Processing } from 'app/components/ui';
import { FieldArray } from 'formik';
import UsersMultiSelect from './UsersMultiSelect';

const ProjectCreateComponent = ({
  account,
  handleSubmit,
  handleChange,
  values,
  options,
  setFieldValue,
}) => (
  <div className="users">
    <Processing processing={false} />

    <section className="hero is-dark">
      <div className="hero-body">
        <div className="container">
          <div className="container">
            <h1 className="is-size-1  title has-text-weight-normal">
              Создать новый проект
            </h1>
          </div>
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        <h3 className="is-size-4 title">Параметры проекта</h3>

        <form onSubmit={handleSubmit} id="project-create-form">
          <div className="columns">
            <div className="column">
              <div className="field">
                <label className="label">Название</label>
                <div className="control">
                  <input
                    className="input"
                    name="name"
                    type="text"
                    onChange={handleChange}
                    value={values.name}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Описание</label>
                <div className="control">
                  <textarea
                    className="textarea"
                    name="description"
                    onChange={handleChange}
                    value={values.description}
                  />
                </div>
              </div>
            </div>

            <div className="column">
              <div className="field">
                <label className="label">Пригласить пользователей</label>
                <div className="control" id="invite-users">
                  <FieldArray
                    name={'invite_users'}
                    render={arrayHelpers => (
                      <UsersMultiSelect
                        account={account}
                        values={values.invite_users}
                        setFieldValue={setFieldValue}
                        options={options}
                      />
                    )}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="form-buttons">
            <button type="submit" className="button primary">
              Создать
            </button>
          </div>
        </form>
      </div>
    </section>
  </div>
);

export default ProjectCreateComponent;
