import * as React from 'react';
import { Pagination } from 'app/components/ui';
import { Processing, Icon } from 'app/components/ui';
import { NavLink } from 'react-router-dom';

const ProjectListComponent = ({
  list: { data, pagination, status },
  pageSet,
}) => (
  <div className="projects">
    <Processing processing={status.processing} />
    <h1 className="is-size-1  title has-text-weight-normal">Проекты</h1>
    {status.loaded && data.length === 0 ? (
      <h1>Пока что нет ни одного проекта</h1>
    ) : (
      <table className="table is-fullwidth">
        <thead>
          <tr>
            <th>Id</th>
            <th>Название</th>
            <td>Создатель</td>
            <td>Описание</td>
          </tr>
        </thead>

        <tbody>
          {data.map(({ id, name, description, creator }) => (
            <tr key={id}>
              <th>{id}</th>
              <td>
                <NavLink to={`/projects/${id}`}>{name}</NavLink>
              </td>
              <td>
                <NavLink to={`/users/${creator.id}`}>
                  {creator.first_name} {creator.last_name}
                </NavLink>
              </td>
              <td>{description}</td>
            </tr>
          ))}
        </tbody>
        <tfoot>
          <tr>
            <td colSpan="4">
              <Pagination pagination={pagination} pageSet={pageSet} />
            </td>
          </tr>
        </tfoot>
      </table>
    )}
  </div>
);

export default ProjectListComponent;
