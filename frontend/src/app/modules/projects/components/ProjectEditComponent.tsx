import * as React from 'react';
import { FieldArray } from 'formik';
import Select from 'react-select';
import { Processing } from 'app/components/ui';
import UsersMultiSelect from './UsersMultiSelect';

const ProjectEditComponent = ({
  detail: { loaded },
  account,
  handleSubmit,
  handleChange,
  values,
  invitationExcludes,
  options,
  setFieldValue,
}) => (
  <div className="project-edit">
    <Processing processing={false} />

    <section className="hero is-dark">
      <div className="hero-body">
        <div className="container">
          <div className="container">
            <h1 className="is-size-1  title has-text-weight-normal">
              Редактирование проекта
            </h1>
          </div>
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        <h3 className="is-size-4 title">Параметры проекта</h3>

        <form onSubmit={handleSubmit} id="project-create-form">
          <div className="columns">
            <div className="column">
              <div className="field">
                <label className="label">Название</label>
                <div className="control">
                  <input
                    className="input"
                    name="name"
                    type="text"
                    onChange={handleChange}
                    value={values.name}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Описание</label>
                <div className="control">
                  <textarea
                    className="textarea"
                    name="description"
                    onChange={handleChange}
                    value={values.description}
                  />
                </div>
              </div>
            </div>

            <div className="column">
              <div className="field">
                <label className="label">Приглашения</label>
                <div className="control" id="invite-users">
                  <FieldArray
                    name={'invite_users'}
                    render={arrayHelpers => (
                      <UsersMultiSelect
                        name="invite_users"
                        excludes={invitationExcludes}
                        account={account}
                        values={values.invite_users}
                        setFieldValue={setFieldValue}
                        options={options}
                      />
                    )}
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Участники</label>
                <div className="control" id="edit-members">
                  <FieldArray
                    name={'members'}
                    render={arrayHelpers => (
                      <Select
                        classNamePrefix="react-select"
                        options={[]}
                        noOptionsMessage={() => 'Ничего не найдено'}
                        onChange={values => setFieldValue('members', values)}
                        complete={true}
                        value={values.members}
                        isMulti={true}
                      />
                    )}
                  />
                </div>
              </div>
            </div>
          </div>

          {loaded && (
            <div className="form-buttons">
              <button type="submit" className="button primary">
                Сохранить
              </button>
            </div>
          )}
        </form>
      </div>
    </section>
  </div>
);

export default ProjectEditComponent;
