import * as React from 'react';
import * as R from 'ramda';
import Select from 'react-select';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withList } from 'app/hoc/list';
import { userApi } from 'app/services/api';

const InviteUsersComponent = ({
  list: { data },
  updateFilter,
  setFieldValue,
  values,
  account,
  name,
}) => (
  <Select
    classNamePrefix="react-select"
    options={R.compose(
      // exclude project creator
      R.without([{ label: account.data.email, value: account.data.id }]),
      R.map(({ id, email }) => ({ label: email, value: id }))
    )(data)}
    noOptionsMessage={() => 'Ничего не найдено'}
    onChange={values => setFieldValue(name, values)}
    complete={true}
    value={values}
    onInputChange={inputValue =>
      updateFilter({ id: 'email', value: inputValue })
    }
    isMulti={true}
  />
);

const InviteUsers = compose(
  connect(({ account }) => ({ account })),
  withList(userApi, {
    filters: [
      {
        id: 'email',
        key: 'email',
        value: '',
        operator: 'startWith',
      },
    ],
  })
)(InviteUsersComponent);
export default InviteUsers;
