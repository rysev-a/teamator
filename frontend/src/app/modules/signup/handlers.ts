import { accountApi } from 'app/services/api';

const handlers = {
  signup: ({ loadAccount }) => ({ values, setSubmitting, setErrors }) =>
    accountApi
      .signup(values)
      .then(() => {
        setSubmitting(false);
        loadAccount();
      })
      .catch(({ response: { data } }) => {
        setSubmitting(false);
        // TODO: generate errors on server
        //setErrors(data.message);
      }),
};

export default handlers;
