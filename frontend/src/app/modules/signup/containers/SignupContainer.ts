import { connect } from 'react-redux';
import { compose, withHandlers } from 'recompose';
import { withFormik } from 'formik';
import SignupComponent from '../components/SignupComponent';
import handlers from '../handlers';
import accountActions from 'app/modules/account/actions';

const SignupContainer = compose(
  connect(
    null,
    dispatch => ({
      loadAccount: () => dispatch(accountActions.load()),
    })
  ),
  withHandlers(handlers),
  withFormik({
    mapPropsToValues: () => ({
      email: '',
      password: '',
      first_name: '',
      last_name: '',
    }),
    handleSubmit: (values, { props, setSubmitting, setErrors }: any) =>
      props.signup({ values, setSubmitting, setErrors }),
  })
)(SignupComponent);

export default SignupContainer;
