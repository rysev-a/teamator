import { push } from 'react-router-redux';
import { accountApi } from 'app/services/api';

const handlers = {
  submit: ({ loadAccount, goHome }) => ({ values, setSubmitting, setErrors }) =>
    accountApi
      .update(values)
      .then(() => {
        setSubmitting(false);
        loadAccount().then(goHome);
      })
      .catch(({ response: { data } }) => {
        setSubmitting(false);
        setErrors(data.message);
      }),
};

export default handlers;
