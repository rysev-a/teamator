import * as React from 'react';
import classNames from 'classnames';
import { Processing } from 'app/components/ui';

const ProfileSettingsComponent = ({
  values,
  errors,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
}) => (
  <div className="profile">
    <h1 className="is-size-1  title has-text-weight-normal title has-text-weight-normal">
      Профиль
    </h1>

    <div className="columns">
      <form className="signin-form column is-half" onSubmit={handleSubmit}>
        <Processing processing={isSubmitting} />

        <div className="field">
          <label className="label">Имя</label>
          <div className="control">
            <input
              className={classNames('input', {
                'is-danger': errors['first_name'],
              })}
              type="text"
              name="first_name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.first_name}
            />
            {errors['first_name'] && (
              <p className="help is-danger">{errors['first_name']}</p>
            )}
          </div>
        </div>

        <div className="field">
          <label className="label">Фамилия</label>
          <div className="control">
            <input
              className={classNames('input', {
                'is-danger': errors['last_name'],
              })}
              type="text"
              name="last_name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.last_name}
            />
            {errors['last_name'] && (
              <p className="help is-danger">{errors['last_name']}</p>
            )}
          </div>
        </div>

        <div className="field">
          <label className="label">Email</label>
          <div className="control">
            <input
              className={classNames('input', {
                'is-danger': errors['email'],
              })}
              type="email"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
            />
            {errors['email'] && (
              <p className="help is-danger">{errors['email']}</p>
            )}
          </div>
        </div>

        <button className="button" type="submit">
          Сохранить
        </button>
      </form>
    </div>
  </div>
);

export default ProfileSettingsComponent;
