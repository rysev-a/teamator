import * as React from 'react';
import { Pagination, Processing, Tag } from 'app/components/ui';
import { NavLink } from 'react-router-dom';

const mapStatusToTag = status => ({
  color: { pending: 'info', rejected: 'danger', success: 'success' }[status],
  children: {
    pending: 'на рассмотрении',
    rejected: 'отклонено',
    success: 'принято',
  }[status],
});

const ProfileInvitationsComponent = ({
  list: { data, pagination, status },
  accept,
  decline,
  pageSet,
}) => (
  <div className="invitations">
    <Processing processing={status.processing} />
    <h1 className="is-size-1  title has-text-weight-normal">
      Приглашения в проекты
    </h1>
    {status.loaded && data.length === 0 ? (
      <h1>Пока что нет ни одного приглашения</h1>
    ) : (
      status.loaded && (
        <table className="table is-fullwidth">
          <thead>
            <tr>
              <th>Id</th>
              <th>Пользователь</th>
              <td>Проект</td>
              <th>Статус</th>
              <th>Действия</th>
            </tr>
          </thead>
          <tbody>
            {data.map(({ id, requesting_user, project, status }) => (
              <tr key={id}>
                <th>{id}</th>
                <td>
                  <NavLink to={`/users/${requesting_user.id}`}>
                    {requesting_user.first_name} {requesting_user.last_name}
                  </NavLink>
                </td>
                <td>
                  <NavLink to={`/projects/${project.id}`}>
                    {project.name}
                  </NavLink>
                </td>
                <td>
                  <Tag {...mapStatusToTag(status)} />
                </td>
                <td>
                  {status === 'pending' && (
                    <div className="is-grouped field">
                      <div className="control">
                        <button
                          className="button is-small is-primary"
                          onClick={accept.bind(null, id)}>
                          Принять
                        </button>
                      </div>
                      <div className="control">
                        <button
                          className="button is-small is-danger"
                          onClick={decline.bind(null, id)}>
                          Отклонить
                        </button>
                      </div>
                    </div>
                  )}
                </td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan="5">
                <Pagination pagination={pagination} pageSet={pageSet} />
              </td>
            </tr>
          </tfoot>
        </table>
      )
    )}
  </div>
);

export default ProfileInvitationsComponent;
