import * as React from 'react';
import { Pagination, Processing } from 'app/components/ui';
import { NavLink } from 'react-router-dom';

const ProfileProjectsComponent = ({
  list: { data, pagination, status },
  pageSet,
  remove,
}) => (
  <div className="projects">
    <Processing processing={status.processing} />
    <h1 className="is-size-1  title has-text-weight-normal">Мои проекты</h1>
    {status.loaded && data.length === 0 ? (
      <h1>Пока что нет ни одного проекта</h1>
    ) : (
      status.loaded && (
        <table className="table is-fullwidth">
          <thead>
            <tr>
              <th>Id</th>
              <td>Название</td>
              <td>Создатель</td>
              <td>Описание</td>
              <td>Действия</td>
            </tr>
          </thead>
          <tbody>
            {data.map(
              ({
                id,
                name,
                description,
                creator: { first_name, last_name },
              }) => (
                <tr key={id}>
                  <th>{id}</th>
                  <td>
                    <NavLink to={`/projects/${id}`}>{name}</NavLink>
                  </td>
                  <td>
                    {first_name} {last_name}
                  </td>
                  <td>{description}</td>
                  <td>
                    <div className="is-grouped field">
                      <div className="control">
                        <NavLink to={`/projects/${id}/edit`}>
                          <button className="button is-small is-primary">
                            Редактировать
                          </button>
                        </NavLink>
                      </div>
                      <div className="control">
                        <button
                          className="button is-small is-danger"
                          onClick={remove.bind(null, id)}>
                          Удалить
                        </button>
                      </div>
                    </div>
                  </td>
                </tr>
              )
            )}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan="5">
                <Pagination pagination={pagination} pageSet={pageSet} />
              </td>
            </tr>
          </tfoot>
        </table>
      )
    )}
  </div>
);

export default ProfileProjectsComponent;
