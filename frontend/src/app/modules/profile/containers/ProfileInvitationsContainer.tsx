import { connect } from 'react-redux';
import { compose, withReducer, withHandlers, lifecycle } from 'recompose';
import { invitationApi } from 'app/services/api';
import { listReducer, createHandlers } from 'app/hoc/list';
import ProfileInvitationsComponent from '../components/ProfileInvitationsComponent';

export const ProfileInvitationsContainer = compose(
  connect(({ account }) => ({ account })),
  withReducer('list', 'localDispatch', listReducer, {
    ...listReducer(undefined, { type: 'init' }),
    ...{
      filters: [
        {
          id: 'receiving_user',
          key: 'receiving_user_id',
          operator: '==',
          value: '',
        },
      ],
      sorting: {
        key: 'id',
        order: 'asc',
      },
    },
  }),
  withHandlers(createHandlers(invitationApi)),
  lifecycle({
    componentDidMount() {
      this.props.updateFilter({
        id: 'receiving_user',
        value: this.props.account.data.id,
      });
    },
  }),
  withHandlers({
    accept: props => id =>
      invitationApi.detail
        .put({ id, values: { status: 'success' } })
        .then(props.load.bind(null, props.list)),
    decline: props => id =>
      invitationApi.detail
        .put({ id, values: { status: 'rejected' } })
        .then(props.load.bind(null, props.list)),
  })
)(ProfileInvitationsComponent);
