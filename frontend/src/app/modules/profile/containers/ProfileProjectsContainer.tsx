import { connect } from 'react-redux';
import { compose, withReducer, withHandlers, lifecycle } from 'recompose';
import { projectApi } from 'app/services/api';
import { listReducer, createHandlers } from 'app/hoc/list';
import ProfileProjectsComponent from '../components/ProfileProjectsComponent';

export const ProfileProjectsContainer = compose(
  connect(({ account }) => ({ account })),
  withReducer('list', 'localDispatch', listReducer),
  withHandlers(createHandlers(projectApi)),
  lifecycle({
    componentDidMount() {
      this.props.initFilters([
        {
          id: 'creator',
          key: 'creator_id',
          operator: '==',
          value: '',
        },
      ]);

      this.props.updateFilter({
        id: 'creator',
        value: this.props.account.data.id,
      });
    },
  })
)(ProfileProjectsComponent);
