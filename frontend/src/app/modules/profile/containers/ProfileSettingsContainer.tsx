import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose, withHandlers } from 'recompose';
import { withFormik } from 'formik';

import handlers from '../handlers';
import actions from '../actions';
import ProfileSettingsComponent from '../components/ProfileSettingsComponent';

export const ProfileSettingsContainer = compose(
  connect(
    ({ account }) => ({ account }),
    dispatch => bindActionCreators(actions, dispatch)
  ),
  withHandlers(handlers),
  withFormik({
    mapPropsToValues: ({ account: { data } }) => ({
      email: data.email || '',
      first_name: data.first_name || '',
      last_name: data.last_name || '',
    }),
    handleSubmit: (values, { props, setSubmitting, setErrors }: any) =>
      props.submit({ values, setSubmitting, setErrors }),
    enableReinitialize: true,
  })
)(ProfileSettingsComponent);
