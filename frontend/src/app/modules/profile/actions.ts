import { push } from 'react-router-redux';
import accountActions from 'app/modules/account/actions';

const profileActions = {
  goHome: () => dispatch => dispatch(push('/')),
  loadAccount: () => dispatch => dispatch(accountActions.load()),
};

export default profileActions;
