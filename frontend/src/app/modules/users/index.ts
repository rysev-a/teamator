import UserListContainer from './containers/UserListContainer';
import UserDetailContainer from './containers/UserDetailContainer';

export { UserListContainer, UserDetailContainer };
