import * as React from 'react';
import { Pagination } from 'app/components/ui';
import { Processing, Icon } from 'app/components/ui';
import { NavLink } from 'react-router-dom';

const UserListComponent = ({ list: { data, pagination, status }, pageSet }) => (
  <div className="users">
    <Processing processing={status.processing} />
    <h1 className="is-size-1  title has-text-weight-normal">Пользователи</h1>
    <table className="table is-fullwidth">
      <thead>
        <tr>
          <th>Id</th>
          <th>Email</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Phone</th>
        </tr>
      </thead>
      <tbody>
        {data.map(({ id, email, first_name, last_name, phone }) => (
          <tr key={id}>
            <th>{id}</th>
            <td>
              <NavLink to={`/users/${id}`}>{email}</NavLink>
            </td>
            <td>{first_name}</td>
            <td>{last_name}</td>
            <td>{phone}</td>
          </tr>
        ))}
      </tbody>
      <tfoot>
        <tr>
          <td colSpan="5">
            <Pagination pagination={pagination} pageSet={pageSet} />
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
);

export default UserListComponent;
