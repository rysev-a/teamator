import * as React from 'react';
import { Processing } from 'app/components/ui';
import UserCard from './UserCard';

const UserDetailComponent = ({
  detail: {
    data: { email, first_name, last_name, phone },
    loaded,
    processing,
  },
}) => (
  <div className="users">
    <Processing processing={processing} />

    <section className="hero is-dark">
      <div className="hero-body">
        <div className="container">
          {loaded && (
            <div className="container">
              <h1 className="is-size-1  title has-text-weight-normal">
                {first_name} {last_name}
              </h1>
            </div>
          )}
        </div>
      </div>
    </section>
    <section className="section">
      <div className="container">
        <UserCard
          email={email}
          first_name={first_name}
          last_name={last_name}
          phone={phone}
        />
      </div>
    </section>
  </div>
);

export default UserDetailComponent;
