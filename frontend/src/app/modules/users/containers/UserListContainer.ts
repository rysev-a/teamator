import UserListComponent from '../components/UserListComponent';
import { userApi } from 'app/services/api';
import { withList } from 'app/hoc/list';

import { compose } from 'recompose';

export default compose(
  withList(userApi, {
    pagination: { count: 15 },
  })
)(UserListComponent);
