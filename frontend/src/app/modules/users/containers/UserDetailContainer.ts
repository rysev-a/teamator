import { compose } from 'recompose';
import { userApi } from 'app/services/api';
import { withDetail } from 'app/hoc/detail';
import UserDetailComponent from '../components/UserDetailComponent';

const UserDetailContainer = compose(withDetail(userApi))(UserDetailComponent);

export default UserDetailContainer;
