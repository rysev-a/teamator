import * as React from 'react';
import classNames from 'classnames';
import { Processing } from 'app/components/ui';

const SigninComponent = ({
  values,
  errors,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
}) => (
  <div className="level">
    <div className="level-item">
      <section className="section">
        <div className="level">
          <h2 className="is-size-4 is-center level-item">Войти на сайт</h2>
        </div>
        <form className="signin-form" onSubmit={handleSubmit}>
          <Processing processing={isSubmitting} />
          <div className="field">
            <label className="label">Email</label>
            <div className="control">
              <input
                className={classNames('input', {
                  'is-danger': errors['email'],
                })}
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              {errors['email'] && (
                <p className="help is-danger">{errors['email']}</p>
              )}
            </div>
          </div>

          <div className="field">
            <label className="label">Password</label>
            <div className="control">
              <input
                className={classNames('input', {
                  'is-danger': errors['password'],
                })}
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              {errors['password'] && (
                <p className="help is-danger">{errors['password']}</p>
              )}
            </div>
          </div>
          <button className="button" type="submit">
            Отправить
          </button>
        </form>
      </section>
    </div>
  </div>
);

export default SigninComponent;
