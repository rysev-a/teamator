import { accountApi } from 'app/services/api';

const handlers = {
  signin: ({ loadAccount }) => ({ values, setSubmitting, setErrors }) =>
    accountApi
      .signin(values)
      .then(() => {
        setSubmitting(false);
        loadAccount();
      })
      .catch(({ response: { data } }) => {
        setSubmitting(false);
        setErrors(data.message);
      }),
};

export default handlers;
