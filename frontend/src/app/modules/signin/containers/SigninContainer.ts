import { compose, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { withFormik } from 'formik';
import SigninComponent from '../components/SigninComponent';
import handlers from '../handlers';

const mapStateToProps = ({ account }) => ({ account });

import accountActions from 'app/modules/account/actions';

const SigninContainer = compose(
  connect(
    mapStateToProps,
    dispatch => ({
      loadAccount: () => dispatch(accountActions.load()),
    })
  ),
  withHandlers(handlers),
  withFormik({
    mapPropsToValues: () => ({ email: '', password: '' }),
    handleSubmit: (values, { props, setSubmitting, setErrors }: any) =>
      props.signin({ values, setSubmitting, setErrors }),
  })
)(SigninComponent);

export default SigninContainer;
